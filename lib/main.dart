import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() => runApp(MyApp());

class RandomWordsState extends State<RandomWords> with TickerProviderStateMixin {
  final Set<WordPair> _saved = Set<WordPair>();
  final WordPair wordPair = WordPair.random();
  final List<WordPair> _suggestions = <WordPair>[];
  final TextStyle _biggerFont = const TextStyle(fontSize: 18);

  Animation<double> _bubblesAnimation;
  AnimationController _savedCountController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.menu), onPressed: _pushSaved),
        ],
      ),
      body: _buildSuggestions(),
    );
  }

  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final Iterable<ListTile> tiles = _saved.map(
              (WordPair pair) {
                return ListTile(
                  title: Text(
                    pair.asPascalCase,
                    style: _biggerFont,
                  ),
                  trailing: CustomPaint(
                    size: Size(24, 24),
                    painter: HeartPainter(true),
                  ),
                );
              }
          );
          final List<Widget> divided = ListTile.divideTiles(
            context: context,
            tiles: tiles,
          ).toList();
          return Scaffold(
            appBar: AppBar(
              title: Text('Saved Suggestions'),
            ),
            body: ListView(children: divided)
          );
        }
      )
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      padding: const EdgeInsets.all(16),
      itemBuilder: (BuildContext _context, int i) {
        if (i.isOdd) return Divider();
        final int index = i ~/ 2;
        if (index >= _suggestions.length) {
          _suggestions.addAll(generateWordPairs().take(10));
        }
        return _buildRow(_suggestions[index]);
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _bubblesAnimation = AnimationController(
      vsync: this, // the SingleTickerProviderStateMixin
      duration: widget.duration,
    );
    _savedCountController = AnimationController(
      vsync: this,
      duration: widget.duration,
    );
  }


  Widget _buildRow(WordPair pair) {
    final bool alreadySaved = _saved.contains(pair);

    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      trailing:  CustomPaint(
        size: Size(24, 24),
        painter: HeartPainter(alreadySaved),
      ),
      onTap: () {
        setState(() {
          if (alreadySaved) {
            _saved.remove(pair);
          } else {
            _saved.add(pair);
          }
        });
      },
    );
  }
}

class HeartPainter extends CustomPainter {
  final bool alreadySaved;

  // @TODO: Animate Heart Icon

  HeartPainter(this.alreadySaved);

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint1 = Paint();

    if (alreadySaved) {
    paint1
      ..color = Colors.red
      ..style = PaintingStyle.fill
      ..strokeWidth = 0;
    } else {
      paint1
        ..color = Colors.grey
        ..style = PaintingStyle.stroke
        ..strokeWidth = 2;
    }

    double width = size.width;
    double height = size.height;

    Path path = Path();
    path.moveTo(0.5 * width, height * 0.38);
    path.cubicTo(0.2 * width, height * 0.1, -0.20 * width, height * 0.6, 0.5 * width, height);
    path.moveTo(0.5 * width, height * 0.38);
    path.cubicTo(0.8 * width, height * 0.1, 1.20 * width, height * 0.6, 0.5 * width, height);

    canvas.drawPath(path, paint1);
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class Sky extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var rect = Offset.zero & size;
    var gradient = RadialGradient(
      center: const Alignment(0.7, -0.6),
      radius: 0.2,
      colors: [const Color(0xFFFFFF00), const Color(0xFF0099FF)],
      stops: [0.4, 1.0],
    );
    canvas.drawRect(
      rect,
      Paint()..shader = gradient.createShader(rect),
    );
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}


class RandomWords extends StatefulWidget {
  RandomWords({ Key key, this.duration }) : super(key: key);
  final Duration duration;

  @override
  RandomWordsState createState() => new RandomWordsState();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.teal,
        textTheme: TextTheme(bodyText1: TextStyle(color: Colors.purple), bodyText2: TextStyle(color: Colors.white)),
      ),
      title: 'Startup Name Generator',
      home: Stack(
        children: <Widget>[
          RandomWords(),
        ],
      )
    );
  }
}